function resetSudoku() {
    //reset sudoku puzzle
    var cell = 1;
    for (var i = 0; i < 81; i++) {
        document.getElementsByName("cell" + cell)[0].value = "";
        cell++;
    }
    document.getElementById("message").innerHTML = "";
    document.getElementById("message").style.color = "black";
}
function findEmptyCell(sudoku) {
    //find empty cell by checking if cell is 0
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            if (sudoku[i][j] == 0) {
                return [i, j];
            }
        }
    }
    return null;
}
function isValid(sudoku, row, col, num) {
    //check if number is in row, column, or subgrid
    for (var i = 0; i < 9; i++) {
        if (sudoku[row][i] == num) {
            return false;
        }
    }
    for (var i = 0; i < 9; i++) {
        if (sudoku[i][col] == num) {
            return false;
        }
    }
    var sub_row = Math.floor(row / 3) * 3;
    var sub_col = Math.floor(col / 3) * 3;
    for (var i = sub_row; i < sub_row + 3; i++) {
        for (var j = sub_col; j < sub_col + 3; j++) {
            if (sudoku[i][j] == num) {
                return false;
            }
        }
    }
    return true;
}

function solve(sudoku) {
    //find empty cell to fill
    var emptyCell = findEmptyCell(sudoku);
    if (emptyCell == null) {
        return sudoku;
    }
    var row = emptyCell[0];
    var col = emptyCell[1];
    //try numbers 1-9
    for (var i = 1; i <= 9; i++) {
        //call isValid function to check if number is valid
        if (isValid(sudoku, row, col, i)) {
            sudoku[row][col] = i;
            //recurively call solve function
            if (solve(sudoku) != null) {
                return sudoku;
            }
            //backtrack if sudoku is invalid
            sudoku[row][col] = 0;
        } 
    }
    return null;
}
function invalid_sudoku_message() {
    document.getElementById("message").innerHTML = "Invalid sudoku puzzle.";
    document.getElementById("message").style.color = "red";
}

function solveSudoku() {
    //create sudoku board using 2d array
    var sudoku = new Array(9);
    for (var i = 0; i < 9; i++) {
        sudoku[i] = new Array(9);
    }
    var cell = 1;
    //row_set, col_set, and subgrid_set are used to check if sudoku is valid
    var row_set = Array.from(Array(9), () => new Set());
    var col_set = Array.from(Array(9), () => new Set());
    var subgrid_set = Array.from(Array(9), () => new Set());

    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            sudoku[i][j] = document.getElementsByName("cell" + cell)[0].value;
            // Check if sudoku is valid
            if (sudoku[i][j] == "") {
                sudoku[i][j] = 0;
            } else {
                // Check if initial sudoku puzzle is valid
                //not a number or 0
                if (sudoku[i][j] == 0 || isNaN(sudoku[i][j])) {
                    invalid_sudoku_message();
                    return;
                }
                //check if number is in row, column, or subgrid
                //if it is, then invalid sudoku puzzle
                if(row_set[i].has(sudoku[i][j]) || col_set[j].has(sudoku[i][j]) || subgrid_set[Math.floor(i / 3) * 3 + Math.floor(j / 3)].has(sudoku[i][j])) {
                    invalid_sudoku_message();
                    row_set = Array.from(Array(9), () => new Set());
                    col_set = Array.from(Array(9), () => new Set());
                    subgrid_set = Array.from(Array(9), () => new Set());
                    return;
                }
                //add number to row, column, and subgrid
                row_set[i].add(sudoku[i][j]);
                col_set[j].add(sudoku[i][j]);
                subgrid_set[Math.floor(i / 3) * 3 + Math.floor(j / 3)].add(sudoku[i][j]);
            }
            cell++;
        }
    }
    //solved sudoku puzzle
    var solvedSudoku = solve(sudoku);
    cell = 1;
    //display solved sudoku puzzle
    for (var i = 0; i < 9; i++) {
        for (var j = 0; j < 9; j++) {
            document.getElementsByName("cell" + cell)[0].value = solvedSudoku[i][j];
            cell++;
        }
    }

}